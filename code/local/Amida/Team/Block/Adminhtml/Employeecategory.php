<?php

class Amida_Team_Block_Adminhtml_Employeecategory extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_employeecategory';
        $this->_blockGroup = 'amida_team';
        $this->_headerText = Mage::helper('amida_team')->__('Category Management');
        $this->_addButtonLabel = Mage::helper('amida_team')->__('Add new category');
        parent::__construct();
    }
}