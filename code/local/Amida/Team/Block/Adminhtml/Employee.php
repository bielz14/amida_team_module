<?php

class Amida_Team_Block_Adminhtml_Employee extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_employee';
        $this->_blockGroup = 'amida_team';
        $this->_headerText = Mage::helper('amida_team')->__('Employees');
        $this->_addButtonLabel = Mage::helper('amida_team')->__('Add new employee');
        parent::__construct();
    }
}