<?php

class Amida_Team_Block_Adminhtml_Employee_Renderer_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $newValue = NULL;
        $value = trim((string)$row->getData($this->getColumn()->getIndex()));
        if (!empty($value)) {
            $category = Mage::getmodel('amida_team/employeecategory')->load($value);
            $newValue = $category->getTitle();
            return $newValue;
        }
    }
}