<?php

class Amida_Team_Block_Adminhtml_Employee_Renderer_Store extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $html;

    public function render(Varien_Object $row)
    {
        $isHeadTitlesView = false;
        if (!(int)$row->getData($this->getColumn()->getIndex())) {
            return false;
        }
        $value = explode(',', trim((string)$row->getData($this->getColumn()->getIndex())));

        $cities = Mage::helper('brander_contactsmod')->getShopCities();
        $shops = Mage::helper('adminforms')->getCollection('offline_shops');
        foreach ($shops as $shop) {
            if (in_array($shop->getEntityId(), $value)) {
                foreach ($cities as $city) {
                    if ($city['city_code'] ==  $shop->getShopCity()) {
                        $storeCity = $city['city_name'];
                        $this->html .= $storeCity . ', ' . $shop->getAdress() . '<br>';
                    }
                }
            }
        }
        return nl2br($this->html);
    }
}