<?php

class Amida_Team_Block_Adminhtml_Employee_Renderer_Manufacturer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $newValue = NULL;
        $value = trim((string)$row->getData($this->getColumn()->getIndex()));
        if (!empty($value)) {
            $attributeId = Mage::getModel('eav/entity_attribute')->getIdByCode('catalog_product', 'manufacturer');
            $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
            $options = $attribute->getSource()->getAllOptions();
            array_shift($options);
            $manufacturers = explode(',', $value);
            foreach ($manufacturers as $manufacturer) {
                foreach ($options as $option) {
                    if ($option['value'] == $manufacturer) {
                        $newValue .= $option['label'] . '<br>';
                    }
                }
            }
            if (!is_null($newValue)) {
                return $newValue;
            }
        }
    }
}