<?php

class Amida_Team_Block_Adminhtml_Employee_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_manufacturer;
    protected $_store;

    public function __construct()
    {
        parent::__construct();
        $this->setId('amidateamEmployeeGrid');
        $this->setDefaultSort('employee_identifier');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amida_team/employee')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('employee_id', array(
            'header'    => Mage::helper('amida_team')->__('ID'),
            'align'     => 'left',
            'index'     => 'employee_id',
            'type'      => 'number',
        ));

        $this->addColumn('firstname', array(
            'header'    => Mage::helper('amida_team')->__('First Name'),
            'align'     => 'left',
            'index'     => 'firstname',
        ));

        $this->addColumn('lastname', array(
            'header'    => Mage::helper('amida_team')->__('Last Name'),
            'align'     => 'left',
            'index'     => 'lastname',
        ));

        $this->addColumn('post', array(
            'header'    => Mage::helper('amida_team')->__('Post'),
            'align'     => 'left',
            'index'     => 'post',
        ));

        $this->addColumn('category', array(
            'header'    => Mage::helper('amida_team')->__('Category'),
            'align'     => 'left',
            'index'     => 'category',
            'renderer' => 'Amida_Team_Block_Adminhtml_Employee_Renderer_Category',
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('amida_team')->__('Email'),
            'align'     => 'left',
            'index'     => 'email'
        ));

        $this->addColumn('phone', array(
            'header'    => Mage::helper('amida_team')->__('Phone'),
            'align'     => 'left',
            'index'     => 'phone'
        ));

        $attributeId = Mage::getModel('eav/entity_attribute')->getIdByCode('catalog_product', 'manufacturer');
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
        $options = $attribute->getSource()->getAllOptions();
        array_shift($options);
        $manufacturerData = [];
        foreach ($options as $option) {
            $manufacturerData[$option['value']] = $option['label'];
        }
        $this->addColumn('manufacturer_ids', array(
            'header'        => Mage::helper('amida_team')->__('Manufacturers'),
            'index'         => 'manufacturer_ids',
            'sortable'      => false,
            'renderer' => 'Amida_Team_Block_Adminhtml_Employee_Renderer_Manufacturer',
            'type' => 'options',
            'options' => $manufacturerData,
            'filter_condition_callback'
                            => array($this, '_filterManufacturerCondition'),
        ));

        $this->addColumn('store_ids', array(
            'header'        => Mage::helper('amida_team')->__('Stores'),
            'index'         => 'store_ids',
            'type'          => 'store',
            'sortable'      => false,
            'filter'    => false,
            'renderer'      => 'Amida_Team_Block_Adminhtml_Employee_Renderer_Store'
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('amida_team')->__('Action'),
            'align'     => 'left',
            'index'     => 'employee_id',
            'type'      => 'action',
            'actions'   =>  array(
                    array(
                        'caption' => Mage::helper('amida_team')->__('Edit'),
                        'url' => array('base'=> '*/*/edit'),
                        'field' => 'employee_id'
                    )
            ),
            'filter'    => false,
            'sortable'  => false
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) 
    {
        return $this->getUrl('*/*/edit', array('employee_id' => $row->getEmployeeId()));
    }

    protected function _filterManufacturerCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $collection->addFieldToFilter('manufacturer_ids', array(
            array('like' => '%,' . $value . ',%'),
            array('like' => '%,'. $value),
            array('like' => $value . ',%'),
            array('like' => $value)
        ));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('employee_id');
        $this->getMassactionBlock()->setFormFieldName('employee_id');
         
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => Mage::helper('tax')->__('Are you sure?')
        ));
         
        return $this;
    }
}