<?php

class Amida_Team_Block_Adminhtml_Employee_Edit_Form_Renderer_Fieldset_Image extends Varien_Data_Form_Element_Abstract
{
    protected $_element;

    public function getElementHtml()
    {   
        $html = '';

        if ($this->getValue()) {
            $url = $this->getValue();

            if(!preg_match("/^http\:\/\/|https\:\/\//", $url) ) {
                $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . ltrim($url, '/');
            }
            
            $html = '<a href="' . $url . '" onclick="imagePreview(\''.$this->getHtmlId() . '_image\'); return false;"><img src="' . $url . '" id="' . $this->getHtmlId() .'_image" title="' . $this->getValue() . '" alt="' . $this->getValue() . '" height="22" width="22" class="small-image-preview v-middle" /></a> ';
        }
        $class = $this->getClass();
        if ($class) {
            $class .= ' ';
        }

        $class .= 'input-file';
    
        $html .= '<input id="image" name="image" type="file" class="input-file">';
        if ($this->getValue()) {
	        $label = Mage::helper('adminhtml')->__('Delete Image');
	        $html .= '<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="' . parent::getName() . '[delete]" value="1" class="checkbox" id="' . $this->getHtmlId() . '_delete"' . ($this->getDisabled() ? ' disabled="disabled"': '') . '/>';
	        $html .= '<label for="' . $this->getHtmlId() . '_delete"' . ($this->getDisabled() ? ' class="disabled"' : '') . '> ' . $label . '</label>';
	        $html .= '<input type="hidden" name="' . parent::getName() . '[value]" value="' . $this->getValue() . '" />';
	    }

        return $html;
    }
}