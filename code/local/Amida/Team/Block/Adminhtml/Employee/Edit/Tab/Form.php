<?php

class Amida_Team_Block_Adminhtml_Employee_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $employee = Mage::registry('employee');
        $form = new Varien_Data_Form(array(
                        'id'         => 'edit_form',
                        'action'     => $this->getUrl('*/*/save', array(
                                'employee_id' => $this->getRequest()->getParam('employee_id')
                            )
                        ),
                        'method'     => 'post',
                        'enctype'    => 'multipart/form-data'
                    )
        );

        $fieldset = $form->addFieldset('employee_fieldset', array('legend' => Mage::helper('amida_team')->__('Employee Information')));

        $fieldset->addType('image', 'Amida_Team_Block_Adminhtml_Employee_Edit_Form_Renderer_Fieldset_Image');

        $fieldset->addField('firstname', 'text', array(
            'name'  => 'firstname',
            'label' => Mage::helper('amida_team')->__('Employee Name'),
            'id'    => 'firstname',
            'title' => Mage::helper('amida_team')->__('Employee Name'),
            'required' => true,
        ));

        $fieldset->addField('lastname', 'text', array(
            'name'  => 'lastname',
            'label' => Mage::helper('amida_team')->__('Employee Lastname'),
            'id'    => 'lastname',
            'title' => Mage::helper('amida_team')->__('Employee Lastname'),
            'required' => true,
        ));

        $fieldset->addField('post', 'text', array(
            'name'  => 'post',
            'label' => Mage::helper('amida_team')->__('Post'),
            'id'    => 'post',
            'title' => Mage::helper('amida_team')->__('Post'),
            'required' => true,
        ));
        
        $categoriesOptions = [];
        array_push($categoriesOptions, null);
        $categories = Mage::getmodel('amida_team/employeecategory')->getCollection();
        foreach ($categories as $category) {
            array_push($categoriesOptions, [
                'value' => $category->getEmployeeCategoryId(),
                'label' => $category->getTitle()
            ]);
        }
        $fieldset->addField('category', 'select', array(
            'name'  => 'category',
            'label' => Mage::helper('amida_team')->__('Category'),
            'id'    => 'category',
            'title' => Mage::helper('amida_team')->__('Category'),
            'required' => true,
            'values' => $categoriesOptions
        ));

        $fieldset->addField('email', 'text', array(
            'name'  => 'email',
            'label' => Mage::helper('amida_team')->__('Email'),
            'id'    => 'customer_email',
            'title' => Mage::helper('amida_team')->__('User Email')
        ));

        $fieldset->addField('phone', 'text', array(
            'name'  => 'phone',
            'label' => Mage::helper('amida_team')->__('Phone'),
            'id'    => 'phone',
            'title' => Mage::helper('amida_team')->__('Phone')
        ));

        $fieldset->addField('image', 'image', array(
            'label'     => Mage::helper('amida_team')->__('Image'),
            'name'      => 'image',
            'align'     => 'left'
        ));

        $attributeId = Mage::getModel('eav/entity_attribute')->getIdByCode('catalog_product', 'manufacturer');
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
        $options = $attribute->getSource()->getAllOptions();
        array_shift($options);
        $manufacturers = [
            0 => [
                'value' => $options
            ]
        ];

        $field = $fieldset->addField('manufacturer_ids', 'multiselect', array(
            'name'   => 'manufacturer_ids[]',
            'label'  => Mage::helper('amida_team')->__('Manufacturer'),
            'title'  => Mage::helper('amida_team')->__('Manufacturer'),
            'values' => $manufacturers,
        ));

        $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
        $field->setRenderer($renderer);

        $fieldset->addField('doors_rating', 'select', array(
            'label' => Mage::helper('amida_team')->__('Doors rating'),
            'title' => Mage::helper('amida_team')->__('Doors rating'),
            'name'  => 'doors_rating',
            'values' => [
                [
                    'value' => 0,
                    'label' => '',
                ],
                [
                    'value' => 1,
                    'label' => Mage::helper('amida_team')->__('One star'),
                ],
                [
                    'value' => 2,
                    'label' => Mage::helper('amida_team')->__('Two stars'),
                ],
                [
                    'value' => 3,
                    'label' => Mage::helper('amida_team')->__('Three stars'),
                ],
            ]
        ));

        $fieldset->addField('floor_rating', 'select', array(
            'label' => Mage::helper('amida_team')->__('Floor rating'),
            'title' => Mage::helper('amida_team')->__('Floor rating'),
            'name'  => 'floor_rating',
            'values' => [
                [
                    'value' => 0,
                    'label' => '',
                ],
                [
                    'value' => 1,
                    'label' => Mage::helper('amida_team')->__('One star'),
                ],
                [
                    'value' => 2,
                    'label' => Mage::helper('amida_team')->__('Two stars'),
                ],
                [
                    'value' => 3,
                    'label' => Mage::helper('amida_team')->__('Three stars'),
                ],
            ]
        ));

        $cities = Mage::helper('brander_contactsmod')->getShopCities();
        $storesOptions = [];
        $stores = Mage::helper('adminforms')->getCollection('offline_shops');
        foreach ($stores as $store) {
            $storeCode = $store->getShopCity();
            foreach ($cities as $city) {
                if ($city['city_code'] == $storeCode) {
                    $storeCity = $city['city_name'];
                    array_push($storesOptions, [
                        'value' => $store->getEntityId(),
                        'label' => $storeCity . ', ' . $store->getAdress()
                    ]);
                }
            }
        }
        $fieldset->addField('store_ids', 'multiselect', array(
            'name'  => 'store_ids[]',
            'label' => Mage::helper('amida_team')->__('Stores'),
            'title' => Mage::helper('amida_team')->__('Stores'),
            'values' => $storesOptions
        ));

        $form->setValues($employee->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}