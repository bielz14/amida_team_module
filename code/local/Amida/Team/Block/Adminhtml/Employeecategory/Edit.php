<?php

class Amida_Team_Block_Adminhtml_Employeecategory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'employee_category_id';
        $this->_controller = 'adminhtml_employeecategory';
        $this->_blockGroup = 'amida_team';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('amida_team')->__('Save Employeecategory'));
        $this->_updateButton('delete', 'label', Mage::helper('amida_team')->__('Delete Employeecategory'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "


            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('employeecategory')->getEmployee_categoryId()) {
            return Mage::helper('amida_team')->__("Edit Employeecategory");
        }
        else {
            return Mage::helper('amida_team')->__('New Employeecategory');
        }
    }

    protected function _prepareLayout()
    {
        $this->setChild('form',
            $this->getLayout()->createBlock( $this->_blockGroup . '/' . $this->_controller . '_edit_tab_form',
            $this->_controller . '._edit_tab_form')->setSaveParametersInSession(true) );
        return parent::_prepareLayout();
    }
}