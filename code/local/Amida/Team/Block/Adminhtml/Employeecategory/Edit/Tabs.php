<?php

class Amida_Team_Block_Adminhtml_Employeecategory_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('employeecategory_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('amida_team')->__('Employeecategory Information'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('main_section', array(
			'label' => Mage::helper('amida_team')->__('Employeecategory Information'),
			'title' => Mage::helper('amida_team')->__('Employeecategory Information')
		));

		return parent::_beforeToHtml();
	}
}