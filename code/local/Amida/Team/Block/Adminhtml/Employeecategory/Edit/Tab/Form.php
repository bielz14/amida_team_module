<?php

class Amida_Team_Block_Adminhtml_Employeecategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $employeecategory = Mage::registry('employeecategory');
        $form = new Varien_Data_Form(array(
                        'id'         => 'edit_form',
                        'action'     => $this->getUrl('*/*/save', array(
                                'employee_category_id' => $this->getRequest()->getParam('employee_category_id')
                            )
                        ),
                        'method'     => 'post',
                        'enctype'    => 'multipart/form-data'
                    )
        );

        $fieldset = $form->addFieldset('employeecategory_fieldset', array('legend' => Mage::helper('amida_team')->__('Employeecategory Information')));

        $fieldset->addField('title', 'text', array(
            'name'  => 'title',
            'label' => Mage::helper('amida_team')->__('Employeecategory Name'),
            'id'    => 'title',
            'title' => Mage::helper('amida_team')->__('Employeecategory Name'),
            'required' => true,
        ));

        $fieldset->addField('position', 'text', array(
            'name'  => 'position',
            'label' => Mage::helper('amida_team')->__('Employeecategory Position'),
            'id'    => 'position',
            'title' => Mage::helper('amida_team')->__('Employeecategory Position'),
            'required' => true,
        ));

        $form->setValues($employeecategory->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}