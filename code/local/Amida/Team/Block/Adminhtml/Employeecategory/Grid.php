<?php

class Amida_Team_Block_Adminhtml_Employeecategory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('amidateamEmployeecategoryGrid');
        $this->setDefaultSort('employeecategory_identifier');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
    	$collection = Mage::getModel('amida_team/employeecategory')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('employee_category_id', array(
            'header'    => Mage::helper('amida_team')->__('ID'),
            'align'     => 'left',
            'index'     => 'employee_category_id',
            'type'      => 'number',
        ));

        $this->addColumn('title', array(
            'header'    => Mage::helper('amida_team')->__('Title'),
            'align'     => 'left',
            'index'     => 'title',
        ));

        $this->addColumn('position', array(
            'header'    => Mage::helper('amida_team')->__('Position'),
            'align'     => 'left',
            'index'     => 'position'
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('amida_team')->__('Action'),
            'align'     => 'left',
            'index'     => 'employee_category_id',
            'type'      => 'action',
            'actions'   =>  array(
                    array(
                        'caption' => Mage::helper('amida_team')->__('Edit'),
                        'url' => array('base'=> '*/*/edit'),
                        'field' => 'employee_category_id'
                    )
            ),
            'filter'    => false,
            'sortable'  => false
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) 
    {   
        return $this->getUrl('*/*/edit', array('employee_category_id' => $row->getEmployee_categoryId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('employee_category_id');
        $this->getMassactionBlock()->setFormFieldName('employee_category_id');
         
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => Mage::helper('tax')->__('Are you sure?')
        ));
         
        return $this;
    }
}