<?php

$employees = Mage::getModel('amida_team/employee')->getCollection();
foreach ($employees as $employee) {
	if ($employee->getEmployeeId() == 1 || $employee->getEmployeeId() == 2) {
		$employee->setCategory(3);
	}
	$employee->setStoreIds(0);
	$employee->save();
}