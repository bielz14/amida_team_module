<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('amida_team/employee')}` (
      `employee_id` int(11) NOT NULL auto_increment,
      `firstname` text NOT NULL,
      `lastname` text NOT NULL,
      `category` text NOT NULL, 
      `post` text NOT NULL,
      `email` text default NULL,
      `phone` varchar(15) default NULL,
      `image` text default NULL,
      `manufacturer_ids` text default NULL,
      `doors_rating` int(1) default NULL,
      `floor_rating` int(1) default NULL,
      `store_ids` text default NULL,
      PRIMARY KEY  (`employee_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

    INSERT INTO `{$installer->getTable('amida_team/employee')}` (`employee_id`, `firstname`, `lastname`, `post`, `category`, `email`, `image`)
    VALUES (1, 'Михаил', 'Абаполов', 'Маркетинг', '', 'marketing@holz.ua', '/skin/frontend/ultimo/unitop/images/employee_photo/abapolov.jpg');        
    INSERT INTO `{$installer->getTable('amida_team/employee')}` (`firstname`, `lastname`, `post`, `category`, `email`, `phone`, `image`) VALUES ('Ольга', 'Волынец', 'HR', '', 'hr@holz.ua', '050 433-54-84', '/skin/frontend/ultimo/unitop/images/employee_photo/laskova.jpg');
    INSERT INTO `{$installer->getTable('amida_team/employee')}` (`firstname`, `lastname`, `post`, `category`, `email`, `image`) VALUES ('Иван', 'Пустовойтенко', 'Категорийный менеджер', 'Двери', 'category.doors@holz.ua', '/skin/frontend/ultimo/unitop/images/employee_photo/pustovoytenko.jpg');  
    INSERT INTO `{$installer->getTable('amida_team/employee')}` (`firstname`, `lastname`, `post`, `category`, `email`, `image`) VALUES ('Анна', 'Ласкова', 'Категорийный менеджер', 'Напольные покрытия', 'category.flooring@holz.ua', '/skin/frontend/ultimo/unitop/images/employee_photo/volynec_bw.jpg');        
");
$installer->run("
    CREATE TABLE `{$installer->getTable('amida_team/employee_category')}` (
      `employee_category_id` int(11) NOT NULL auto_increment,
      `title` text,
      `position` int(6),
      PRIMARY KEY  (`employee_category_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

    INSERT INTO `{$installer->getTable('amida_team/employee_category')}` (`employee_category_id`, `title`, `position`)
    VALUES (1, 'Консультанты', 5);        
    INSERT INTO `{$installer->getTable('amida_team/employee_category')}` (`title`, `position`)
    VALUES ('Управляющая команда', 10);
");
$installer->endSetup();