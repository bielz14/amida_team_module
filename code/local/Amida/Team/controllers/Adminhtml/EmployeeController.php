<?php

class Amida_Team_Adminhtml_EmployeeController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('amida_team/adminhtml_employee'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction() 
    {
        $id = $this->getRequest()->getParam('employee_id');
        Mage::register('employee', Mage::getModel('amida_team/employee')->load($id));
        $employeeObject = (array)Mage::getSingleton('adminhtml/session')->getEmployeeObject(true);
        if (count($employeeObject)) {
            Mage::registry('employee')->setData($employeeObject);
        }
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('amida_team/adminhtml_employee_edit'))
        ->_addLeft($this->getLayout()->createBlock('amida_team/adminhtml_employee_edit_tabs'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('employee_id');
            $model = Mage::getModel('amida_team/employee')->load($id);
            if (!$model->getEmployeeId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amida_team')->__('This employee no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            $data['employee_id'] = $id;

            $manufacturers = implode(',', $this->getRequest()->getPost('manufacturer_ids')); 
            $data['manufacturer_ids'] = $manufacturers;

            $stores = implode(',', $this->getRequest()->getPost('store_ids')); 
            $data['store_ids'] = $stores;

            try {
                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                    $uploader = new Varien_File_Uploader('image');
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $path = Mage::getBaseDir('media') .  '/employee_photo';
                    $fileName = $_FILES['image']['name'];
                    $uploader->save($path, $fileName);
                    $data['image'] = '/media/employee_photo/' . $fileName;
                } else if((isset($data['image']['delete']) && $data['image']['delete'] == 1)){
                    unlink(MAGENTO_ROOT . $data['image']['value']);
                    $data['image'] = '';
                } else if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }

                $model->setData($data);
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amida_team')->__('The employee has been saved.'));
                
                if ($this->getRequest()->getParam('back')) {
                    Mage::getSingleton('adminhtml/session')->setEmployeeObject($data);
                    $this->_redirect('*/*/edit', array('employee_id' => $id));
                    return;
                }
                
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setEmployeeObject(false);

                $this->_redirect('*/*/edit', array('employee_id' => $this->getRequest()->getParam('employee_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $employee = Mage::getModel('amida_team/employee')
            ->setId($this->getRequest()->getParam('employee_id'))
            ->delete();
        if ($employee->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amida_team')->__('Employee was deleted successfully!'));
        }
        $this->_redirect('*/*/');

    }

    public function massDeleteAction() {
        $requestIds = $this->getRequest()->getParam('employee_id');
        if(!is_array($requestIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select reqeust(s)'));
        } else {
            try {
                foreach ($requestIds as $requestId) {
                    $RequestData = Mage::getModel('amida_team/employee')->load($requestId);                    
                    $RequestData->delete();                    
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($requestIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}