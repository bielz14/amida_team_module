<?php

class Amida_Team_Adminhtml_EmployeecategoryController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('amida_team/adminhtml_employeecategory'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction() 
    {
        $id = $this->getRequest()->getParam('employee_category_id');
        Mage::register('employeecategory', Mage::getModel('amida_team/employeecategory')->load($id));
        $employeecategoryObject = (array)Mage::getSingleton('adminhtml/session')->getEmployeecategoryObject(true);
        if (count($employeecategoryObject)) {
            Mage::registry('employeecategory')->setData($employeecategoryObject);
        }
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('amida_team/adminhtml_employeecategory_edit'))
        ->_addLeft($this->getLayout()->createBlock('amida_team/adminhtml_employeecategory_edit_tabs'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('employee_category_id');
            $model = Mage::getModel('amida_team/employeecategory')->load($id);
            if (!$model->getEmployee_categoryId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amida_team')->__('This employeecategory no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            $data['employee_category_id'] = $id;

            try {

                $model->setData($data);
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amida_team')->__('The employeecategory has been saved.'));
                
                if ($this->getRequest()->getParam('back')) {
                    Mage::getSingleton('adminhtml/session')->setEmployeecategoryObject($data);
                    $this->_redirect('*/*/edit', array('employee_category_id' => $id));
                    return;
                }
                
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setEmployeecategoryObject(false);

                $this->_redirect('*/*/edit', array('employee_category_id' => $this->getRequest()->getParam('employee_category_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $employee = Mage::getModel('amida_team/employeecategory')
            ->setId($this->getRequest()->getParam('employee_category_id'))
            ->delete();
        if ($employee->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amida_team')->__('Employeecategory was deleted successfully!'));
        }
        $this->_redirect('*/*/');

    }

    public function massDeleteAction() {
        $requestIds = $this->getRequest()->getParam('employee_category_id');
        if(!is_array($requestIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select reqeust(s)'));
        } else {
            try {
                foreach ($requestIds as $requestId) {
                    $RequestData = Mage::getModel('amida_team/employeecategory')->load($requestId);                    
                    $RequestData->delete();                    
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($requestIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}