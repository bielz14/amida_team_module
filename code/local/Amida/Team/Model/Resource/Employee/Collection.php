<?php

class Amida_Team_Model_Resource_Employee_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('amida_team/employee');
    }
}
	 