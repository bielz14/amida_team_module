<?php

class Amida_Team_Model_Resource_Employeecategory extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('amida_team/employeecategory', 'employee_category_id');
    }
}